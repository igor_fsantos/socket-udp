#include "funcoes.h"

void encerrar(char *s){
	perror(s);
    exit(1);
}

int cria_socket_UDP(struct sockaddr_in *si, int si_tam, int porta){
	int s;

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        encerrar("socket");
    }

    //Preenche a estrutura com zeros
    memset((char *)si, 0, si_tam);
    
    //Atribuicoes padroes p/ estrutura de um socket UDP
    si->sin_family = AF_INET;
    si->sin_port = htons(porta); //Porta vinculada ao socket
    si->sin_addr.s_addr = htonl(INADDR_ANY);

    return s;
 }

//Funcao generica para fazer o checksum
unsigned short soma_verif(unsigned short *ptr, int nbytes) 
{
    register long soma;
    unsigned short byte_impar;
    register short resposta;
 
    soma = 0;
    while(nbytes > 1) {
    	//O conteudo do ponteiro e' um valor numerico porque e' utilizado sempre um cast (unsigned short *)
        soma += *ptr++; //Fazendo aritmetica de ponteiro e atribuindo o conteudo do ponteiro à variavel soma
        nbytes-=2;
    }
    if(nbytes == 1) {
        byte_impar = 0;
        *((u_char*)&byte_impar) = *(u_char*)ptr; //Capturando o conteudo do ultimo byte do ponteiro
        soma += byte_impar; //Somando o conteudo do ultimo byte captura na variavel soma
    }
    // ==== Operacoes efetuadas com a variavel soma --- PARA PALAVRAS DE 16 bits ====
    //_(soma & 0xffff) => operacao bitwise, comparando cada bit de soma com cada bit do numero em binario 1111111111111111
    soma = (soma >> 16) + (soma & 0xffff);
    soma = soma + (soma >> 16); //_(soma >> 16) => equivalente à soma/2^16
    resposta = (short)~soma; //_(short)~soma => complemento de 2 da variavel soma
     
    return(resposta);
}

//Funcao para inicializar os valores do pacote
struct pacote inicializaPacote(void){
	struct pacote pac;

	pac.csum = 0;
	pac.numSequencia = 0;
	pac.numReconhecimento = 0;
	memset(pac.mensagem, 0, TAM_BUF);
	memset(pac.arquivo, 0, NOME_ARQ);
	pac.flag = CON_PAC;

	return pac;
}

//Funcao que cria o nome do arquivo a ser reconstruido
void arqReconstruido(char *nomeArq){
	int i = 0;
	char c = nomeArq[i];
	while(c != '.' && c != '\0'){
		i++;
		c = nomeArq[i];
	}
	nomeArq[i] = '\0'; //Removendo o '\n' do nome do arquivo
	strcat(nomeArq, "_COPIA.txt");

	return;
}
//Menu que exibe as funcoes que o rastreador pode realizar
void menuRastreador(void){
    printf("\n=== RASTREADOR ===\n");
    printf("Entre com o digito da acao que deseja executar:\n");
    printf("1. Inserir informacao no arquivo\n");
    printf("2. Receber pedidos dos clientes\n");
    printf("-1. Fechar o rastreador\n");

    return;
}
//Menu que exibe as funcoes do cliente
void menuCliente(void){
    printf("\n=== CLIENTE ===\n");
    printf("Entre com o digito da acao que deseja executar:\n");
    printf("1. Consultar o rastreador para obter o arquivo\n");
    printf("2. Obter o arquivo direto do servidor\n");
    printf("-1. Fechar o cliente\n");

    return;
}
//Le o arquivo de dados com informacoes dos nós e os arquivos existentes
void buscaServidorArq(char *nomeArq, char *ipServidor){
	FILE *fp;
	char c;
	char nomeArqTemp[NOME_ARQ];
	int i = 0;
	if((fp = fopen(BANCO_DADOS, "r")) == NULL){
		perror(BANCO_DADOS);
		memset(ipServidor, 0, TAM_STR_SERV);
		return;
	}else{
		while ((c = getc(fp)) != EOF){
			if(c == '('){
				while ((c = getc(fp)) != ':'){ //Depois do ':' o arquivo contem o endereco do servidor
					nomeArqTemp[i] = c;
					i++;
				}
				nomeArqTemp[i] = '\0'; //Final da string
				i = 0;
				if(strcmp(nomeArq, nomeArqTemp) == 0){ //Comparando o nome do arquivo do banco com o arquivo pedido pelo usuario
					while ((c = getc(fp)) != ')'){
						ipServidor[i] = c;
						i++;
					}
					ipServidor[i] = '\0';
					return;
				}else memset(nomeArqTemp, 0, NOME_ARQ);
			}
		}
	}
	printf("O arquivo pedido nao foi encontrado!\n");
	memset(ipServidor, 0, TAM_STR_SERV);
	fclose(fp);
	return;
}
//Permite a insercao de novos arquivos no banco de dados
void insereBancoDados(void){
	FILE *fp;
	char c;
	char nomeArq[NOME_ARQ], ipServidor[TAM_STR_SERV];
	if((fp = fopen(BANCO_DADOS, "a+")) == NULL){
		perror(BANCO_DADOS);
		return;
	}else{
		fseek(fp, 0, SEEK_END); //Indo para o final do arquivo
		if(ftell(fp) > 0){ //Verificando se o arquivo nao esta vazio
			c = '\n'; //Caso nao esteja vazio, entao uma linha e' pulada
			fprintf(fp, "%c", c);
		}
		printf("Entre com o nome do arquivo: ");
		scanf("%s", nomeArq);
		printf("Entre com o endereco do servidor: ");
		scanf("%s", ipServidor);
		c = '(';
		fprintf(fp, "%c", c);
		fprintf(fp, "%s", nomeArq);
		c = ':';
		fprintf(fp, "%c", c);
		fprintf(fp, "%s", ipServidor);
		c = ')';
		fprintf(fp, "%c", c);
	}
	fclose(fp);
	printf("\n");
	return;
}
