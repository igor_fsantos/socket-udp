#include "funcoes.h"

int main(int argc, char const *argv[])
{
	//Respectivamente: Struct de socket do servidor e struct de socket do cliente
	struct sockaddr_in si_proprio, si_outro;
    //Variaveis de controle da criacao do socket 
    int s, s_tam = sizeof(si_outro), recup_tam;

    //Temporizador
    struct timeval temporizador;  

 	//Atribuindo os valores do temporizador
 	temporizador.tv_sec = TIMEOUT;
    temporizador.tv_usec = 0; 

    //
    char nomeArq[NOME_ARQ], ipServidor[TAM_STR_SERV];

    s = cria_socket_UDP((struct sockaddr_in *)&si_proprio, s_tam, PORT_RAST);

    //Vincula o socket à porta
    if(bind(s, (struct sockaddr*)&si_proprio, s_tam) == -1)
    {
        encerrar("bind");
    }

    short opc;
    while(1){
    	menuRastreador();
    	scanf("%hd", &opc);
    	if(opc == 1){
    		insereBancoDados(); //Insere novas informacoes de nos no bando de dados do rastreador
    	}else if(opc == 2){
    		break;
    	}else if(opc == -1){
    		printf("Finalizando...\n");
    		close(s);
    		exit(1);
    	}else{
    		printf("Opcao invalida!\n");
    	}
    }
    while(1){ //Permanece no loop recebendo pedidos
        printf("Aguardando pedidos...\n");
        fflush(stdout);

        //Tenta receber os pedidos
        if ((recup_tam = recvfrom(s, nomeArq, NOME_ARQ, 0, (struct sockaddr *)&si_outro, (socklen_t*)&s_tam)) < 0)
        {
            encerrar("recvfrom()");
        }else{
        	printf("Pedido recebido de %s:%d\n", inet_ntoa(si_outro.sin_addr), ntohs(si_outro.sin_port));
        	printf("Arquivo pedido: %s\n", nomeArq);
        	buscaServidorArq(nomeArq, ipServidor);
        	printf("Endereco do servidor encontrado: %s\n", ipServidor);
        	//Enviando o pacote para o cliente
    		setsockopt (s, SOL_SOCKET, SO_SNDTIMEO, (char *)&temporizador, sizeof(temporizador));
    		if (sendto(s, ipServidor, recup_tam,  0, (struct sockaddr *)&si_outro, s_tam) < 0)
    		{
        		printf("Estouro de tempo no envio do pacote\n");
        		perror("A funcao sendto falhou");
    		}else{
    			printf("O endereco do servidor foi enviado com sucesso para: %s:%d\n", inet_ntoa(si_outro.sin_addr), ntohs(si_outro.sin_port));
    		}
        }
    }

    close(s);
	return 0;
}