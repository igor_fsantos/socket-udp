#ifndef FUNCOES_H
#define FUNCOES_H

#include <unistd.h> //close();
#include <stdio.h> //printf
#include <string.h> //memset
#include <stdlib.h> //exit(0);
#include <arpa/inet.h> //inet_aton();
#include <sys/socket.h> //Providencia declaracoes de sockets
#include <errno.h> //errno - o numero do erro
#include <netinet/udp.h> //Providencia declaracoes para o cabeçalho udp
#include <netinet/ip.h>  //Providencia declaracoes para o cabeçalho ip

#define SERVIDOR "127.0.0.1" //Endereco do servidor local - Mude o endereco para acessar um servidor remoto
#define TIMEOUT 5 //Tempo maximo de espera do temporizador
#define END_RAST "127.0.0.1" //Endereco do rastreador local - Mude o endereco para acessar um rastreador remoto
#define BANCO_DADOS "bancoDados.txt" //Arquivo de texto que o rastreador ira' consultar
#define TAM_STR_SERV 17 //Tamanho da string que ira armazenar os enderecos dos servidores
#define TAM_PACOTE 256 //Os pacotes foram definidos como pacotes de 256 bytes
#define FIM_PAC '0' //Flags = '0'indicam que o pacote e' o ultimo a ser enviado
#define CON_PAC '1' //Flags = '1' indicam que ainda tem mais pacotes a serem enviados daquele mesmo arquivo
#define NOME_ARQ 32 //Comprimento do nome do arquivo
#define TAM_BUF 209 //Tamanho maximo do buffer
#define PORT 8888 //A porta usada pelo servidor
#define PORT_RAST 8889 //A porta usada pelo rastreador

struct pacote
{
	unsigned short csum;
	unsigned int numSequencia;
	unsigned int numReconhecimento;
	char flag;
	char arquivo[NOME_ARQ];
	char mensagem[TAM_BUF];	
};

void encerrar(char *s);
//Cria um socket UDP
int cria_socket_UDP(struct sockaddr_in *si, int si_tam, int porta);
//Funcao generica que faz a soma de verificacao
unsigned short soma_verif(unsigned short *ptr, int nbytes);
//Funcao para inicializar os valores do pacote
struct pacote inicializaPacote(void);
//Funcao que cria o nome do arquivo a ser reconstruido
void arqReconstruido(char *nomeArq);
//Menu que exibe as funcoes que o rastreador pode realizar
void menuRastreador(void);
//Menu que exibe as funcoes do cliente
void menuCliente(void);
//Le o arquivo de dados com informacoes dos nós e os arquivos existentes
void buscaServidorArq(char *nomeArq, char *ipServidor);
//Permite a insercao de novos arquivos no banco de dados
void insereBancoDados(void);

#endif