#include "funcoes.h"

int main(int argc, char const *argv[])
{
	//Respectivamente: Struct de socket do servidor e struct de socket do cliente
	struct sockaddr_in si_proprio, si_outro;
	//Variavel que ira' armazenar o endereco e a porta do cliente temporariamente
	unsigned long enderClienteTemp = 0;
	unsigned short portaTemp = 0;
    //Variaveis de controle da criacao do socket 
    int s, s_tam = sizeof(si_outro);
    int recup_tam; //Tamanho do pacote recebido do cliente

    //Arquivo de leitura
    FILE *fp = NULL;

    //Pacote a ser recebido
    struct pacote pacote;

    //Temporizador
    struct timeval temporizador;  

 	//Atribuindo os valores do temporizador
 	temporizador.tv_sec = TIMEOUT;
    temporizador.tv_usec = 0; 

    s = cria_socket_UDP((struct sockaddr_in *)&si_proprio, s_tam, PORT);

    //Vincula o socket à porta
    if(bind(s, (struct sockaddr*)&si_proprio, s_tam) == -1)
    {
        encerrar("bind");
    }

    //Mantem o servidor recebendo pedidos
    while(1){
        printf("Aguardando pedidos...\n");
        fflush(stdout);  
        
        //Tenta receber os pacotes
        if ((recup_tam = recvfrom(s, &pacote, sizeof(struct pacote), 0, (struct sockaddr *)&si_outro, (socklen_t*)&s_tam)) < 0)
        {
            encerrar("recvfrom()");
        }else{ //Pacote recebido com sucesso
        	printf("Pedido recebido de %s:%d\n", inet_ntoa(si_outro.sin_addr), ntohs(si_outro.sin_port));
        	//Caso seja um cliente diferente pedindo um outro pacote
        	if((enderClienteTemp != si_outro.sin_addr.s_addr) || (portaTemp != si_outro.sin_port)){ 
        		if(fp != NULL) fclose(fp); //Se algum outro arquivo estiver aberto, e' necessario fecha-lo para fazer uma outra leitura
        		if((fp = fopen(pacote.arquivo, "r")) == NULL){ //Abrindo o arquivo para a leitura
        			perror(pacote.arquivo);
        			memset(pacote.arquivo, 0, NOME_ARQ); //Limpando o nome do arquivo para informar ao cliente que o arquivo nao foi encontrado
        		}else{
        			enderClienteTemp = si_outro.sin_addr.s_addr; //Salvando o endereco do cliente
        			portaTemp = si_outro.sin_port; //Salvando a porta do cliente
        		}
        	}
        	//Lendo o arquivo a partir da posicao determinada
        	if(fp != NULL){
        		int pacotesEnviados = pacote.numSequencia / TAM_PACOTE; //Contabilizando o numero de pacotes enviados
        		long pos = (pacotesEnviados * TAM_BUF); //Encontrando a posicao de leitura do arquivo
        		if(pos < 0) pos = 0; //Caso seja a primeira posicao do arquivo
        		fseek(fp, pos, SEEK_SET); //Determinando a posicao de leitura do arquivo

                char c; //Variavel para capturar cada caractere da mensagem
                int count = 0; //Variavel de contagem do tamanho da mensagem
                //O pacote tem sua mensagem preenchida ate' que o numero de caracteres lidos seja menor que o tamanho do buffer
                //e ainda nao chegou no fim do arquivo
                while (count < TAM_BUF && (c = getc(fp)) != EOF) {
                    pacote.mensagem[count++] = c;
                }
                pacote.mensagem[count] = '\0'; //Indicando o final da string
        		unsigned short tam_mensagem = strlen(pacote.mensagem);
        		if(tam_mensagem < TAM_BUF - 1){ //Ultima parte da mensagem foi lida
        			pacote.flag = FIM_PAC;
        			printf("\n======= ENVIO DO ULTIMO PACOTE =======\n");
        		}else{
        			pacote.flag = CON_PAC;
        		}

        		pacote.csum = soma_verif((unsigned short *)pacote.mensagem, tam_mensagem); //Aplicando a soma de verificacao
        		pacote.numSequencia += TAM_PACOTE; //Incrementando o numero de sequencia
        	}
        	//Enviando o pacote para o cliente
    		setsockopt (s, SOL_SOCKET, SO_SNDTIMEO, (char *)&temporizador, sizeof(temporizador));
    		if (sendto(s, &pacote, recup_tam,  0, (struct sockaddr *)&si_outro, s_tam) < 0)
    		{
        		printf("Estouro de tempo no envio do pacote\n");
        		perror("A funcao sendto falhou");
    		}else{
    			unsigned short numPac = pacote.numSequencia/TAM_PACOTE;
    			printf("Pacote enviado: %d <===> Tamanho do pacote: %ld\n", numPac, sizeof(struct pacote));
    		}
        }   	
    }

    if(fp != NULL) fclose(fp);
    close(s);
	return 0;
}