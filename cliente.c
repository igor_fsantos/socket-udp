#include "funcoes.h"

int main(int argc, char const *argv[])
{
	struct sockaddr_in si_outro;
    int s = -1, s_tam = sizeof(si_outro);
    
    //pacote a ser enviado
    struct pacote pacote;

    //Inicilizando os valores do pacote
    pacote = inicializaPacote();

    //Temporizador
    struct timeval temporizador;  

 	//Atribuindo os valores do temporizador
 	temporizador.tv_sec = TIMEOUT;
    temporizador.tv_usec = 0;

    //Variaveis para controlar a sequencia dos pacotes recebidos
    unsigned int numReconhecimento = 0;   
    //Variaveis para controlar o recebimento dos pacotes
    unsigned int numPac = 0, numFalhas = 0; 

    //Nome temporario de arquivos para efetuar os pedidos
    char tempNome[NOME_ARQ];
    char ipServidorTemp[TAM_STR_SERV];
    char ipServidor[TAM_STR_SERV]; //O servidor no qual o cliente ira' conectar para fazer o pedido direto do pacote

    short opc;
    while(1){ 
    	//Nome do arquivo que sera' pedido ao servidor
    	memset(tempNome, 0, NOME_ARQ);
    	printf("Entre com o nome do arquivo: ");
    	scanf("%s", tempNome);
    	menuCliente();
    	scanf("%hd", &opc);
    	if(opc == 1){ //Comunicacao com o rastreador
    		s = cria_socket_UDP((struct sockaddr_in *)&si_outro, s_tam, PORT_RAST); //A porta na qual o rastreador se encontra e' 8889
    		if (inet_aton(END_RAST, &si_outro.sin_addr) == 0) //Conexao atraves do endereco do rastreador
    		{
    			close(s);
        		fprintf(stderr, "A funcao inet_aton() falhou\n");
        		exit(1);
    		}
    		while(numFalhas < 3){ //O numero maximo de falhas permitidas e' 3
    			//Pedindo o arquivo para o servidor
    			setsockopt (s, SOL_SOCKET, SO_SNDTIMEO, (char *)&temporizador, sizeof(temporizador));
    			//Informando ao rastreador o nome do arquivo que deseja
    			if (sendto(s, tempNome, NOME_ARQ,  0, (struct sockaddr *)&si_outro, sizeof(si_outro)) < 0) 
    			{
    				numFalhas++;
        			printf("Estouro de tempo no envio de pedido de pacote\n");
        			perror("A funcao sendto falhou");
    			}else{
            		//Tenta receber o endereco do servidor
            		setsockopt (s, SOL_SOCKET, SO_RCVTIMEO, (char *)&temporizador, sizeof(temporizador));
            		//Obtendo a resposta do rastreador, com o possivel ip do servidor que contem o arquivo
            		if (recvfrom(s, ipServidorTemp, TAM_STR_SERV, 0, (struct sockaddr *)&si_outro, (socklen_t*)&s_tam) < 0)
            		{
            			numFalhas++;
            			printf("Estouro de tempo no recebimento do pacote\n");
                		perror("recvfrom()");
                	}else{
                		numFalhas = 0;
                		strcpy(ipServidor, ipServidorTemp); //Salvando o possivel ip do servidor que contem o arquivo
                		break;
                	}
                }
            }
            close(s);
            if(strlen(ipServidor) == 0){ //O banco de dados do rastreador nao tem registro de algum servidor que contem o arquivo pedido
            	printf("O arquivo pedido nao foi encontrado!\n");
            }else break; //O rastreador forneceu um ip de servidor que contem o arquivo pedido
    	}else if(opc == 2){ //Caso o cliente ja saiba qual servidor que contem o arquivo desejado
    		strcpy(ipServidor, SERVIDOR); //O ip do servidor ja' se encontra previamente salvo na constante SERVIDOR
    		break;
    	}else if(opc == -1){
    		printf("Finalizando...\n");
    		exit(1);
    	}else{ //O usuario efetuou alguma operacao diferente de 1, 2, -1
    		printf("Opcao invalida!\n");
    	}
    }

    strcpy(pacote.arquivo, tempNome); //Salvando o nome do arquivo a ser recebido dentro da struct do pacote

    s = cria_socket_UDP((struct sockaddr_in *)&si_outro, s_tam, PORT); //Criando um socket na porta 8888
    if (inet_aton(ipServidor, &si_outro.sin_addr) == 0) //Conectando-se ao servidor que possivelmente contem o arquivo
    {
    	close(s);
        fprintf(stderr, "A funcao inet_aton() falhou\n");
        exit(1);
    }

    //Preparando o arquivo a ser reconstruido
    FILE *fp;
    memset(tempNome, 0, NOME_ARQ);
    strcpy(tempNome, pacote.arquivo);
   	arqReconstruido(tempNome);
    if((fp = fopen(tempNome, "w+")) == NULL){
    	encerrar(tempNome);
    }
    while(1){ //Permanece no loop ate' receber o arquivo inteiro
    	//Pedindo o arquivo para o servidor
    	setsockopt (s, SOL_SOCKET, SO_SNDTIMEO, (char *)&temporizador, sizeof(temporizador));
    	if (sendto(s, &pacote, TAM_PACOTE,  0, (struct sockaddr *)&si_outro, sizeof(si_outro)) < 0)
    	{
    		numFalhas++;
        	printf("Estouro de tempo no envio de pedido de pacote\n");
        	perror("A funcao sendto falhou");
    	}else{ //O pedido chegou ao servidor com sucesso
    		printf("Pedido enviado para %s:%d\n", inet_ntoa(si_outro.sin_addr), ntohs(si_outro.sin_port));
            //Tenta receber os pacotes
            setsockopt (s, SOL_SOCKET, SO_RCVTIMEO, (char *)&temporizador, sizeof(temporizador));
            if (recvfrom(s, &pacote, sizeof(struct pacote), 0, (struct sockaddr *)&si_outro, (socklen_t*)&s_tam) < 0)
            {
            	numFalhas++;
            	printf("Estouro de tempo no recebimento do pacote\n");
                perror("recvfrom()");
            }else{ //Pacote recebido
            	//Verificando a integridade do pacote recebido
            	unsigned short csum = soma_verif((unsigned short *)pacote.mensagem, strlen(pacote.mensagem));
            	if((csum != pacote.csum) || (numReconhecimento != pacote.numReconhecimento) || (numReconhecimento + TAM_PACOTE != pacote.numSequencia)){
            		numFalhas++;
            		printf("O pacote nao foi recebido corretamente!\n");
            		if(strlen(pacote.arquivo) == 0){ //O servidor nao possui o arquivo que foi pedido
            			fclose(fp);
            			encerrar("O servidor nao possui o arquivo que foi pedido!\n");
            		}
            	}else{ //O pacote foi recebido corretamente 
            		numPac++;
            		numFalhas = 0;
            		printf("O pacote %d foi recebido corretamente!\n", numPac);
            		printf("Pacote recebido de %s:%d\n", inet_ntoa(si_outro.sin_addr), ntohs(si_outro.sin_port));
            		fprintf(fp, "%s", pacote.mensagem); //Reconstruindo a mensagem do arquivo            		
            		if(pacote.flag == FIM_PAC){ //Verificando se e' o ultimo pacote recebido
            			printf("\n======= TODOS OS PACOTES FORAM RECEBIDOS CORRETAMENTE =======\n");
            			break; 
            		} 
            		numReconhecimento += TAM_PACOTE;
            	}
            	//Preparando o recebimento do proximo pacote
            	memset(pacote.mensagem, 0, TAM_BUF);
            	pacote.numSequencia = numReconhecimento;
            	pacote.numReconhecimento = numReconhecimento;
            }
        }
        if(numFalhas == 3){//Numero maximo de falhas permitidas
            printf("Problemas com o recebimento do pacote %d\n", numPac + 1);
            encerrar("Ocorreram 3 falhas seguidas durante o recebimento desse pacote!\n");
        } 
    }
    fclose(fp);
    close(s);

	return 0;
}